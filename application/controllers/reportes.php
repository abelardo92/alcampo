<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportes extends CI_Controller {

	//Listado
	public function index()
	{
		$this->load->model('moguardia');
		$this->load->model('moproductos');
		$this->moguardia->isin();
		$d['e'] = 0;
		if($this->moguardia->permitido('consultar'))
		{
			//Inicialización
			$d['controlador'] = $this->router->class;
			$d['permisos'] = array(
				'registrar' => $this->moguardia->permitido('registrar'),
				'cancelar' => $this->moguardia->permitido('cancelar'),
				'eliminar' => $this->moguardia->permitido('eliminar')
			);
			
			//Filtros
			$filtro = trim($this->input->post('filtro', true));
			$fecha_venta1 = trim($this->input->post('fecha_venta1', true));
			$fecha_venta2 = trim($this->input->post('fecha_venta2', true));
			$serie = trim($this->input->post('serie', true));
			$go = (int)$this->input->post('go', true);

			//Todos los productos
			$d['todos'] = $this->moproductos->productos_todos();
			
			//Líneas
			$d['lineas'] = array();
			$lineas = $this->moproductos->lineas();
			foreach($lineas as $l)
			{
				$d['lineas'][] = array(
					'nombre' => $l,
					'columnas' => count($this->moproductos->productos_en_linea($l))
				);
			}
			
			//Obtener ventas
			$ventas = array();
			if($go > 0)
			{
				$ventas = $this->obtenerVentas($filtro,$serie,$fecha_venta1,$fecha_venta2);
			}
			
			$d['registros'] = $ventas;
			
			//Info del usuario (Obtener serie)
			$d['series'] = $this->moguardia->series();
			
			//Reasignar valores para su devolución
			$d['f'] = array(
				'filtro' => $filtro,
				'fecha_venta1' => $fecha_venta1,
				'fecha_venta2' => $fecha_venta2,
				'serie' => $serie
			);
		}
		else
		{
			$d['e']++;
			$d['msg'] = 'No cuenta con los permisos suficientes para el acceso a este módulo.';
		}
		
		$h['titulo'] = "Reporte de ventas";
		$d['titulo'] = $h['titulo'];
		$this->load->view('lay/includes.html', $h);
		$this->load->view('lay/header.html');
		$this->load->view('reporte.html', $d);
		$this->load->view('lay/footer.html');
	}

	function obtenerVentas($filtro, $serie, $fecha_venta1, $fecha_venta2) 
	{

		//Valores para el modelo
		$w = '';
		if(strlen($filtro) > 0) $w .= " AND (folio LIKE '{$filtro}' OR serie = UPPER('{$filtro}') OR CONCAT(serie, '-', folio) = UPPER('{$filtro}') OR cliente = '{$filtro}' OR nombre LIKE '%{$filtro}%')";
		if(strlen($serie) > 0) $w .= " AND serie = '{$serie}'";
		
		//Filtros de fecha
		if(strlen($fecha_venta1) > 0 and strlen($fecha_venta2) > 0){ $w .= " AND fecha_venta BETWEEN '{$fecha_venta1}' AND '{$fecha_venta2}'"; } else { $fecha_venta1 = date('Y-m-d'); $fecha_venta2 = date('Y-m-d'); }

		//Todos los productos
		$d['todos'] = $this->moproductos->productos_todos();

		$s = "SELECT v.id, v.serie, v.folio, c.nombre, c.direccion, v.fecha_venta FROM ventas v
		INNER JOIN clientes c ON v.cliente = c.id
		WHERE v.status = 0 AND v.serie IN ('" . implode("','", $this->moguardia->series()) . "') {$w}";
		$ventas = $this->db->query($s)->result_array();
		foreach($ventas as $k => $r)
		{
			foreach($d['todos'] as $p)
			{
				$s = "SELECT SUM(v.cantidad) cantidad, v.registro FROM ventas_productos AS v
							INNER JOIN productos AS p ON v.producto = p.id
							WHERE v.venta = {$r['id']} AND p.categoria = {$p->id} GROUP BY p.categoria, v.registro";
				
				$q = $this->db->query($s);
				if($q->num_rows() > 0)
				{
					$productos = $q->result_array();
					$ventas[$k]['productos'][$p->id] = $productos;
				}
			}
		}
		return $ventas;
	}
	
	//Exportar a PDF
	function exportar()
	{
		$d['tabla'] = trim($this->input->post('tabla', true));
		$f = trim($this->input->post('fecha', true));
		$this->db->query('SET lc_time_names = "es_MX"');
		$d['fecha'] = $this->db->query("SELECT UPPER(DATE_FORMAT('{$f}', '%d de %M de %Y')) f")->row()->f;
		
		// $d['tabla'] = str_replace('class="text-right"', 'align="right"', $d['tabla']);
		$d['tabla'] = str_replace('56', '52', $d['tabla']);
		$d['tabla'] = preg_replace('/<\/?a[^>]*>/','', $d['tabla']);
		
		$h['titulo'] = "Reporte de ventas";
		$d['titulo'] = $h['titulo'];
		$this->load->view('lay/includes.html', $h);
		$this->load->view('reporte_export.html', $d);
		$this->load->view('lay/footer.html');
	}

	function exportarExcel() 
	{
		require_once(APPPATH.'libraries/PHPExcel/PHPExcel.php');
		$this->load->model('moguardia');
		$this->load->model('moproductos');

		//Filtros
		$filtro = trim($this->input->post('filtro', true));
		$fecha_venta1 = trim($this->input->post('fecha_venta1', true));
		$fecha_venta2 = trim($this->input->post('fecha_venta2', true));
		$serie = trim($this->input->post('serie', true));

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("ALCAMPO")
									->setLastModifiedBy("ALCAMPO")
									->setTitle("Documento de ventas de ALCAMPO")
									->setSubject("Documento de ventas de ALCAMPO")
									->setDescription("Documento de ventas de ALCAMPO")
									->setKeywords("Ventas")
									->setCategory("Ventas");

		// Set active Sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Merge Cells
		$objPHPExcel->getActiveSheet()->mergeCells('A1:D1')->mergeCells('E1:J1')->mergeCells('K1:O1');

		// Creating styles for headers
		$styleAlignedBold = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'font' => array(
				'bold' => true
			)
		);

		$styleBold = array(
			'font' => array(
				'bold' => true
			)
		);

		$styleAlignedVertical = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
	
		// Settings styles
		$objPHPExcel->getActiveSheet()->getStyle("A1:O1")->applyFromArray($styleAlignedBold);
		$objPHPExcel->getActiveSheet()->getStyle("A2:O2")->applyFromArray($styleBold);

		// Add first line
		$objPHPExcel->getActiveSheet()
					->setCellValue('E1', 'Cartucho escopeta')
					->setCellValue('K1', 'Fuego central');


		// Add Headers
		$objPHPExcel->getActiveSheet()
					->setCellValue('A2', 'Folio.')
					->setCellValue('B2', 'Nombre')
					->setCellValue('C2', 'Dirección')
					->setCellValue('D2', 'Registro')
					->setCellValue('E2', '22')
					->setCellValue('F2', '12')
					->setCellValue('G2', '16')
					->setCellValue('H2', '20')
					->setCellValue('I2', '28')
					->setCellValue('J2', '410')
					->setCellValue('K2', 'Cal. .22 Mag')
					->setCellValue('L2', 'Cal. .25')
					->setCellValue('M2', 'Cal. .32')
					->setCellValue('N2', '380')
					->setCellValue('O2', '38 SPL');

		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Reporte de ventas');

		$ventas = $this->obtenerVentas($filtro, $serie, $fecha_venta1, $fecha_venta2);

		$c = 3;
		foreach($ventas as $v)
		{
			$objPHPExcel->getActiveSheet()
						->setCellValue("A$c", $serie == '' ? "{$v['serie']}-{$v['folio']}" : $v['folio'])
						->setCellValue("B$c", $v['nombre'])
						->setCellValue("C$c", $v['direccion']);
			// $c++;

			/*
			1. Recorrer los productos de esta venta.
			2. Recorrer los registros de ese producto.
			3. Recorrer todos los productos del catálogo para ver dónde se va a colocar.
			Luego buscar en esta venta si uno de esos tipos coincide.
			*/
			$originalCont = $c;
			$todos = $this->moproductos->productos_todos();

			foreach($v['productos'] as $pid => $producto)
			{
				foreach($producto as $p)
				{
					$objPHPExcel->getActiveSheet()->setCellValue("D$c", $p['registro']);
					$cell = 'E';
					// Set all sale quantities in respective cell.
					foreach($todos as $t)
					{
						if($pid == $t->id)
						{
							$totales[$pid] += $p['cantidad'];
							$objPHPExcel->getActiveSheet()->setCellValue("{$cell}{$c}", $p['cantidad']);
						}
						// Increment cell (Example: E => F => G => H) ...
						$cell++; 
					}
					$c++;
				}
			}

			// Merge cells vertically when customers has more than one register
			if(count($v['productos']) > 1) {
				$objPHPExcel->getActiveSheet()->mergeCells("A{$originalCont}:A".($c - 1))->mergeCells("B{$originalCont}:B".($c-1))->mergeCells("C{$originalCont}:C".($c-1));
			}

		}

		// Formulas to get report totals
		$limit = $c - 1;
		$objPHPExcel->getActiveSheet()->setCellValue("D$c","Totales");
		$objPHPExcel->getActiveSheet()->getStyle("D$c")->applyFromArray($styleBold);
		$objPHPExcel->getActiveSheet()->getStyle("A3:O$limit")->applyFromArray($styleAlignedVertical);

		// $cell = 'E';
		for($cell = 'E'; $cell <= 'O'; $cell++) {
			$objPHPExcel->getActiveSheet()->setCellValue("{$cell}$c","=SUM({$cell}3:{$cell}$limit)");
			if($cell == 'K') {
				$objPHPExcel->getActiveSheet()->getColumnDimension($cell)->setAutoSize(false)->setWidth("12");
			} else {
				$objPHPExcel->getActiveSheet()->getColumnDimension($cell)->setAutoSize(false)->setWidth("10");
			}
		}

		// sum totals

		// $objPHPExcel->getActiveSheet()->setCellValue("P$limit","Total general");
		// $objPHPExcel->getActiveSheet()->setCellValue("P$c","=SUM(E$c:O$c)");
		// $objPHPExcel->getActiveSheet()->getStyle("P$limit")->applyFromArray($styleBold);

		$totalLine = $c + 1;
		$objPHPExcel->getActiveSheet()->setCellValue("D$totalLine","Total general");
		$objPHPExcel->getActiveSheet()->getStyle("D$totalLine")->applyFromArray($styleBold);
		$objPHPExcel->getActiveSheet()->setCellValue("E$totalLine","=SUM(E$c:O$c)");

		// for($cont = 1; $cont < $c; $cont++) {
		// 	$objPHPExcel->getActiveSheet()->getRowDimension($cont)->setRowHeight(20);
		// }

		$objPHPExcel->getActiveSheet()->getStyle("A1:O$c")->getAlignment()->setWrapText(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(false)->setWidth("8");
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(false)->setWidth("30");
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false)->setWidth("40");
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false)->setWidth("11");
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(false)->setWidth("20");

		// $objPHPExcel->getActiveSheet()->setAutoFilter("A1:O1");
		// $objPHPExcel->getActiveSheet()->setAutoFilter("A2:O{$limit}");
		// $objPHPExcel->getActiveSheet()->setAutoFilter("A2:A{$limit}");
		// $objPHPExcel->getActiveSheet()->setAutoFilter(
		// 	$objPHPExcel->getActiveSheet()->calculateWorksheetDimension()
		// );
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Reporte de Ventas.xlsx"');
		header('Cache-Control: max-age=0');

		// If you're serving to IE 9, then the following may be needed
		// header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
		exit;

	}
}